import Exceptions.ExtensionIsNotHandledException;
import org.apache.commons.io.FilenameUtils;
import org.junit.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class fileDiferentiatorTest {

//    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
//    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
//    private final PrintStream originalOut = System.out;
//    private final PrintStream originalErr = System.err;
//
//    @Before
//    public void setUpStreams() {
//        System.setOut(new PrintStream(outContent));
//        System.setErr(new PrintStream(errContent));
//    }
//
//    @After
//    public void restoreStreams() {
//        System.setOut(originalOut);
//        System.setErr(originalErr);
//    }

    @Test
    public void shouldBeEquals() throws IOException, ExtensionIsNotHandledException {

        HashMap<String, List<String>> extensionsAndMagicNumbers = new HashMap<>();
        extensionsAndMagicNumbers.put("PNG",Collections.singletonList("89504E470D0A1A0A"));
        extensionsAndMagicNumbers.put("JPG", Arrays.asList("FFD8FFDB", "FFD8FFE000104A4649460001","FFD8FFEE","FFD8FFE1"));
        extensionsAndMagicNumbers.put("GIF", Arrays.asList("474946383761", "474946383961"));
        extensionsAndMagicNumbers.put("DOCX", Arrays.asList("504B0304"));
        extensionsAndMagicNumbers.put("TXT", Collections.singletonList(""));


//        Path file = Paths.get("src/1.png");
//        String extension = FilenameUtils.getExtension("src/1.png");
//        byte[] fileArray;
//        fileArray = Files.readAllBytes(file);
//        String hex =  Main.bytesToHex(fileArray);
//        Main.fileDifferentiator(extensionsAndMagicNumbers,extension,hex);

//        System.out.print("a PNG file is a PNG file");
//        assertEquals(Main.fileDifferentiator(extensionsAndMagicNumbers,extension,hex),outContent.toString());

    }
}
