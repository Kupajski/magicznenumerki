import Exceptions.ExtensionIsNotHandledException;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class FileDifferentiator {

    public FileDifferentiator() {
    }

    private  HashMap<String, List<String>> extensionsAndMagicNumbers(){

         /*Map which contains extension as a key and magic number (or numbers) created
        taken from:
             - https://en.wikipedia.org/wiki/List_of_file_signatures
             - https://www.filesignatures.net/index.php?page=search
         */

        HashMap<String, List<String>> extensionsAndMagicNumbers = new HashMap<>();
        extensionsAndMagicNumbers.put("PNG",Collections.singletonList("89504E470D0A1A0A"));
        extensionsAndMagicNumbers.put("JPG", Arrays.asList("FFD8FFDB", "FFD8FFE000104A4649460001","FFD8FFEE","FFD8FFE1"));
        extensionsAndMagicNumbers.put("GIF", Arrays.asList("474946383761", "474946383961"));
        extensionsAndMagicNumbers.put("DOCX", Arrays.asList("504B0304"));
        extensionsAndMagicNumbers.put("PDF", Arrays.asList("255044462d"));
        extensionsAndMagicNumbers.put("TXT", Collections.singletonList(""));

        return extensionsAndMagicNumbers;
    }

    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();
    //Function that change an array of Bytes to the String in Hex
    public String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            //We don't have to change the whole array, because magic numbers appears at the beggining but I left it.
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    private boolean searchingForKeyHavingValue(String extension, String hex){
        HashMap<String, List<String>> map = extensionsAndMagicNumbers();
        for (String o : map.keySet()) {
            for(String magicNumbers : map.get(o)) {
                if ((magicNumbers.equals(hex.substring(0, magicNumbers.length())) &&  magicNumbers.length()!=0) || extension.equals("TXT")) {
                    System.out.println("Extension is " + extension + " , while actually it's a " + o);
                    return true;
                }
            }
        }
        return false;
    }

    //Final function
    public void fileDifferentiator(String extension, String hex) throws ExtensionIsNotHandledException {

        HashMap<String, List<String>> map = extensionsAndMagicNumbers();
        extension = extension.toUpperCase();
        List<String> ext = map.get(extension);
        boolean flag = false;

        if(ext == null || ext.size()==0){
            throw new ExtensionIsNotHandledException();
        }else if(ext.size()==1){
            // I am checking if magicnumber from website is equal to the tested one
            if(ext.get(0).equals(hex.substring(0, ext.get(0).length())) || extension.equals("TXT")){
                System.out.println("a " + extension + " file is a " + extension + " file");
                /*if there is no compatible magicnumber i am searching through the map to find a key (extension)
                by the magicnumbers in the tested file
                */
            }else if(!searchingForKeyHavingValue(extension,hex)){
                //if there is no such key, i throw an exception
                throw new ExtensionIsNotHandledException();
            }
        }else{
            //same case as above but here, we tested more than one magicnumber connected with extension
            for(String magicNumbers : ext){
                if((magicNumbers.equals(hex.substring(0,magicNumbers.length())) && magicNumbers.length()!=0)  || extension.equals("TXT")){
                    System.out.println("a " + extension + " file is a " +extension + " file");
                    flag = true;
                }
            }
            if(!flag && !searchingForKeyHavingValue(extension,hex)){
                throw new ExtensionIsNotHandledException();
            }
        }
    }
}
