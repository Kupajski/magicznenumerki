import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import Exceptions.ExtensionIsNotHandledException;
import org.apache.commons.io.FilenameUtils;
import java.nio.file.Paths;
import java.util.*;

public class Main {


    public static void main(String[] args) throws IOException, ExtensionIsNotHandledException {


        Scanner scanner = new Scanner(System.in);
        FileDifferentiator fD = new FileDifferentiator();

        System.out.println("Transfer the file that you want to be tested to src/ folder");
        System.out.println("Type the name of file:");
        String fileName = scanner.nextLine();

        //File loaded and its extension seperated
        Path file = Paths.get("src/"+ fileName);
        String extension = FilenameUtils.getExtension("src/" + fileName);

        //Expressing loaded file by the Array of a Bytes
        byte[] fileArray;
        fileArray = Files.readAllBytes(file);
        String hex =  fD.bytesToHex(fileArray);

        //File Differantiator
        fD.fileDifferentiator(extension,hex);



    }
}
